var fecha = require('./fecha.js');

//console.log(fecha.fecha());

var puerto = 3000;
var express = require('express');
var app = express();

app.get('/', function(req, res) {
  res.send(fecha.fecha());
});

app.listen(puerto, function() {
  console.log("Servidor corriendo en el puerto " + puerto + ".");
});
