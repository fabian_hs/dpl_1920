// Suma
exports.suma = function(numero1,numero2){return numero1+numero2};

// Resta
exports.resta = function(numero1,numero2){return numero1-numero2};

//Modulo
exports.modulo = function(numero1,numero2){return numero1%numero2};

// multiplicacion
exports.multiplicacion = function(numero1,numero2){return numero1*numero2};

// Division
exports.division = function(numero1,numero2){return numero1/numero2};
